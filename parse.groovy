import groovy.json.*
import groovy.json.internal.LazyMap

props = new ArrayList<>()

def folder = "jsons"

new File(folder).eachFile { file ->
    file.each { string ->
        new JsonSlurper().parseText(string).each { key, value ->
            recurseCalc(value, key as String)
        }
    }
}

def res = props.groupBy {it}.sort { a, b ->
    a.value.size() <=> b.value.size()
}

res.each {p ->
    println(p.key +":"+p.value.size())
}

def recurseCalc(def v, def keyName){
    if (v.getClass() == LazyMap){
        def map = v.entrySet()
        map.each {entry ->
            if (entry.getValue().getClass() != String){
                recurseCalc(entry.getValue(), keyName+"."+entry.getKey())
            }else{
                path = keyName+"."+entry.getKey()
                props << path.replaceAll(/basket.\d+/,"basket")
            }
        }
    }else{
        props << keyName
    }
}
